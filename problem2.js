// Problem 2:

// Using callbacks and the fs module's asynchronous functions, do the following:



//     4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
//     5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
function operations(){
let fs = require("fs");

//1. Read the given file lipsum.txt
fs.readFile('../lipsum.txt', 'utf8', (err, data) => {
    if (err) {
        return console.error(err)
    }
    //console.log(data)
    //     2. Convert the content to uppercase & write to a new file. Store the name of the new file in filenames.txt
    let uppercase = data.toUpperCase();
    fs.writeFile("./file1.txt", uppercase, function (err) {
        if(err){
            return console.error(err)
        }
        fs.writeFile('./filenames.txt', 'file1.txt ', function (err) {
            if (err) {
                return console.error(err);
            }
            //console.log("Data written successfully!");
        })
//     3. Read the new file and convert it to lower case. Then split the contents into sentences. Then write it to a new file. Store the name of the new file in filenames.txt
        fs.readFile('file1.txt', 'utf8', (err, data) =>{
            if (err) {
                        throw err;
                    }
                
                    
                    data.toLowerCase();
            
                    let lowercase = data.split("\n");
                     lowercase=JSON.stringify(lowercase)
                 
                  fs.writeFile("file2.txt", lowercase, function (err) {

                    if(err)
                    {
                        throw err
                    }
                    //console.log("Data written successfully!");
                    
                    fs.appendFile('filenames.txt', 'file2.txt ', function (err) {
                            if (err) {
                                throw err
                            }
                        
                            //console.log("Data written successfully!");
                        
                        
                    
                     //4. Read the new files, sort the content, write it out to a new file. Store the name of the new file in filenames.txt
                
                     fs.readFile("./file1.txt", 'utf8', function (err, data) {
                            
                        console.log(data)

                        const arrData = data.split("");
                        
                        
                        arrData.sort();

                        let arrData1=JSON.stringify(arrData)

                        fs.writeFile("file3.txt",arrData1, function (err) {
                            if(err)
                            {
                                throw err;
                            }

                            fs.appendFile("filenames.txt", "file3.txt", function (err) {
                                
                                if(err)
                                {
                                    throw err;
                                }

                                //5. Read the contents of filenames.txt and delete all the new files that are mentioned in that list simultaneously.
                                fs.readFile("filenames.txt", "utf8", function (err, data) {

                                    if(err)
                                    {
                                        throw err;
                                    }

                                    let arr = data.split(" ");

                                    for(let i=0;i<arr.length;i++)
                                    {
                                        //console.log(arr)
            
                                        fs.unlink(`${arr[i]}`, (err) => {

                                            if(err)
                                            {
                                                throw err;
                                            }

                                            //console.log(`${arr[i]} deleted successfully`)
                                        })
                                    }
                                })
                            })
                        })
                    })
                })
                                    
                                

                  

    

                                
                            
                    
                    })
                })



        
        
    })
})

}
module.exports=operations;